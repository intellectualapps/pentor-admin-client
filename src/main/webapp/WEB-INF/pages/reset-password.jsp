    <%@page contentType="text/html" pageEncoding="UTF-8" %>
        <!DOCTYPE html>
        <html>
        <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags
        -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="./favicon.ico">

        <title>Pentor password reset</title>
        <!-- Bootstrap core CSS -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet">

        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <link href="assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="assets/css/dashboard.css" rel="stylesheet">
        <link href="assets/css/styles.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        </head>
            <%
        String email = (String)request.getAttribute("email");
        String token = (String)request.getAttribute("token");
        if (email == null) {
            email = "";
        }
        if (token == null) {
            token = "";
        }
    %>
        <body>
        <div class="container">
        <div id="passwordreset" style="margin-top:50px" class="mainbox col-md-6 col-md-offset-3 col-sm-8
        col-sm-offset-2">
        <div class="panel panel-primary">
        <div class="panel-heading">
        <div class="panel-title">Create New Password</div>
        </div>
        <div class="panel-body">
        <div id="message-wrapper"></div>
        <form id="signupform" class="form-horizontal" role="form">
        <input type="hidden" name="encoded-email" id="encoded-email" value="<%= email %>">
        <input type="hidden" name="password-reset-token" id="password-reset-token" value="<%= token %>">
        <div class="form-group">
        <label for="email" class=" control-label col-sm-3">New password</label>
        <div class="col-sm-9">
        <input type="password" class="form-control" name="user-new-password" placeholder="enter your new password"
        id="user-new-password" required="">
        </div>
        </div>
        <div class="form-group">
        <label for="email" class=" control-label col-sm-3">Confirm password</label>
        <div class="col-sm-9">
        <input type="password" name="user-new-password-confirm" class="form-control"placeholder="Confirm new password"
        required="" id="user-new-password-confirm">
        </div>
        </div>
        <div class="form-group">
        <!-- Button -->
        <div class=" col-sm-offset-4 col-sm-9">
        <button type="button" class="btn btn-success"id="reset-password-button">Reset Password</button>
        </div>
        </div>
        </form>
        </div>
        </div>
        </div>
        </div>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="assets/js/jquery.min.js"><\/script>')</script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/util.js"></script>
        <script src="assets/js/index.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
        </body>
        </html>
